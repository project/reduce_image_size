<?php

namespace Drupal\reduce_image_size\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ReduceImageSizeForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reduce_image_size_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['reduce_image_size.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['maxWidth'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum width'),
      '#description' => $this->t('Enter the maximum width for the image in pixels.'),
      '#required' => TRUE,
      '#min' => 1,
      '#default_value' => $this->config('reduce_image_size.settings')->get('maxWidth') ?? 800,
    ];

    $form['quality'] = [
      '#type' => 'number',
      '#title' => $this->t('Quality compress'),
      '#description' => $this->t('Enter the quality level for image compression. This value should be between 1 (lowest quality) and 100 (highest quality).'),
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 100,
      '#default_value' => $this->config('reduce_image_size.settings')->get('quality') ?? 80,
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('reduce_image_size.settings')
      ->set('maxWidth', $form_state->getValue('maxWidth'))
      ->set('quality', $form_state->getValue('quality'))
      ->save();

    $this->messenger()->addMessage($this->t('Image size reduction settings have been saved successfully.'));
  }
}
